FirefoxControl Library
======================

This library allows user to get and select tabs of running instance of Firefox browser. It was written with xmonad's gridselect in mind. 
Installed and running MozRepl Extension (https://addons.mozilla.org/en-us/firefox/addon/mozrepl/) is required.

Also, you need to set path to init.js in extensions.mozrepl.initUrl in about:config of Firefox. 

Example of usage:
   
```
#!haskell

 selectFirefoxTab :: X ()
    selectFirefoxTab = do
        (tabs, conn, repl) <- liftIO $ getTabs
        let stabs = map (\(x,y) -> (U.toString y,x)) tabs
        selected <- gridselect myGridSelectConfig stabs
        when (isJust selected) $ do
                let Just s = selected
                liftIO $ CF.selectTab conn repl s 
        liftIO $ closeConn conn repl 
        return ()
        where
            getTabs = do
                conn <- connectTo "localhost" $ PortNumber $ 4242
                hSetBuffering conn LineBuffering
                repl <- CF.getReplName conn :: IO B.ByteString
                tabs <- CF.genTabsList conn repl
                return (tabs, conn, repl)
            closeConn conn repl = do
                B.hPutStrLn conn (repl `B.append` ".quit()")
                hClose conn
```