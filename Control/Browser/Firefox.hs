{-# LANGUAGE OverloadedStrings #-}
module Control.Browser.Firefox
where
import System.IO
import qualified Data.ByteString.Char8 as B
import Network
import Control.Monad
import Data.Char
import Data.Monoid

until_t ::  Monad m => m (Bool, t) -> (t -> m b) -> m b
until_t cond body = go
    where go = do
            (c, t)<- cond
            if (c) then do
                body t
            else
                go

while_tlc :: (MonadPlus m1, Monad m) =>Bool -> m (Bool, t) -> (t -> m t1) -> m (m1 t1)
while_tlc llast cond body = go
    where go = do
            (c, t)<- cond
            if (c) then do
                x <- body t
                xs <- go 
                return (return x `mplus` xs)
            else
                if llast then do
                    x<- body t
                    return (return x) 
                else 
                    return mzero

while_t :: (MonadPlus m1, Monad m) => m (Bool, t) -> (t -> m t1) -> m (m1 t1)
while_t cond body =  while_tlc False cond body
while_tl :: (MonadPlus m1, Monad m) => m (Bool, t) -> (t -> m t1) -> m (m1 t1)
while_tl cond body =  while_tlc True cond body

sendCommandWithReply :: Handle -> B.ByteString-> IO B.ByteString
sendCommandWithReply handle command = do
    B.hPutStrLn handle command
    r <- while_t (do
        x <- B.hGet handle 1
        let y = (x /= ">")
        return (y,x))
        return :: IO [B.ByteString] 
    return $ B.unlines $ init $ B.lines (mconcat r) 
    
getReplName ::  Handle -> IO B.ByteString
getReplName handle = do
    r<- until_t (do
        x<- while_tl (do
            e<-B.hGet handle 1
            return (e /= "\n" && e /= ">" ,e))
            $ \x -> do
                return x
        let c = B.elemIndex '>' (mconcat x)
        let y = if (c == Nothing) then False else B.take 4 (mconcat x) ==  "repl"
        return (y,x)) $
        \x -> do
            let d = B.takeWhile (/= '>') (mconcat x)
            return d
    return $ r

genTabsList conn repl = do
    _<-sendCommandWithReply conn  $ "tabs=" `B.append` repl `B.append` ".tabs(document)"
    b<-sendCommandWithReply conn $ repl `B.append` ".showTabs(tabs)"
    let bs = map ( \x -> (B.takeWhile (not.isSpace) x, B.tail $ B.dropWhile (not.isSpace) x)) $ B.lines $ B.dropWhile isSpace b
    let hlist = (\(x,y)-> (B.tail x,y)) $ head $ filter selectedCondition bs  
    let tlist = filter (not.selectedCondition) bs  
    return (hlist:tlist)
    where selectedCondition x = B.head (fst x) == 's'

selectTab conn repl n = do
    _<-sendCommandWithReply conn  $ "tabs=" `B.append` repl `B.append` ".tabs(document)"
    let command = repl `B.append` ".setCurrentTabIndex( tabs, " `B.append` n `B.append` ")"
    B.putStrLn command
    b<-sendCommandWithReply conn $ command
    return ()


main ::  IO ()
main = do
    conn <- connectTo "localhost" $ PortNumber $ 4242
    hSetBuffering conn LineBuffering
    repl <- getReplName conn :: IO B.ByteString
    B.putStrLn repl
    genTabsList conn repl
    selectTab conn repl "3"
    B.hPutStrLn conn (repl `B.append` ".quit()")
    hClose conn
