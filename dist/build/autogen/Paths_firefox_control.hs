module Paths_firefox_control (
    version,
    getBinDir, getLibDir, getDataDir, getLibexecDir,
    getDataFileName, getSysconfDir
  ) where

import qualified Control.Exception as Exception
import Data.Version (Version(..))
import System.Environment (getEnv)
import Prelude

catchIO :: IO a -> (Exception.IOException -> IO a) -> IO a
catchIO = Exception.catch


version :: Version
version = Version {versionBranch = [0,1,0,0], versionTags = []}
bindir, libdir, datadir, libexecdir, sysconfdir :: FilePath

bindir     = "/home/keyran/.cabal/bin"
libdir     = "/home/keyran/.cabal/lib/x86_64-linux-ghc-7.8.3/firefox-control-0.1.0.0"
datadir    = "/home/keyran/.cabal/share/x86_64-linux-ghc-7.8.3/firefox-control-0.1.0.0"
libexecdir = "/home/keyran/.cabal/libexec"
sysconfdir = "/home/keyran/.cabal/etc"

getBinDir, getLibDir, getDataDir, getLibexecDir, getSysconfDir :: IO FilePath
getBinDir = catchIO (getEnv "firefox_control_bindir") (\_ -> return bindir)
getLibDir = catchIO (getEnv "firefox_control_libdir") (\_ -> return libdir)
getDataDir = catchIO (getEnv "firefox_control_datadir") (\_ -> return datadir)
getLibexecDir = catchIO (getEnv "firefox_control_libexecdir") (\_ -> return libexecdir)
getSysconfDir = catchIO (getEnv "firefox_control_sysconfdir") (\_ -> return sysconfdir)

getDataFileName :: FilePath -> IO FilePath
getDataFileName name = do
  dir <- getDataDir
  return (dir ++ "/" ++ name)
