function tabs(document){
    return document.getElementsByTagName("tabs")[0];
}

function setCurrentTabIndex (tabs, index){
    tabs.selectedItem = tabs.childNodes[index];
}

function listVisibleTabsNames(tabs){
    a = []
    for (i=0; i< tabs.childNodes.length; i+=1){
        if (!tabs.childNodes[i].hidden)
        a.push({number:i, label : tabs.childNodes[i].label, tab:tabs.childNodes[i]});
    }
    return a;
}

function showTabs (tabs){
    a = listVisibleTabsNames (tabs);
    for (i=0; i< a.length; i+=1){
        if (a[i].tab.selected) s = "s" 
        else s = "";
        repl.print(s+a[i].number+" "+a[i].label);
    }
}
